const boxing = () => `
<section class="Boxing">
<div class="image-boxing"><img src="../../../assets/images/boxing.png" alt="image-boxing"></div>
<div class="boxing-description">
  <h3>Whatever you get in the box</h3>
  <ul class="ul-boxing">
    <li class="li-boxing"><img src="../../../assets/icons/arrow-in-circle.svg" alt="arrow-in-circle.svg">5A Charger</li>
    <li class="li-boxing"><img src="../../../assets/icons/arrow-in-circle.svg" alt="arrow-in-circle.svg">Extra battery</li>
    <li class="li-boxing"><img src="../../../assets/icons/arrow-in-circle.svg" alt="arrow-in-circle.svg">Sophisticated bag</li>
    <li class="li-boxing"><img src="../../../assets/icons/arrow-in-circle.svg" alt="arrow-in-circle.svg">User manual guide</li>
  </ul>
</div>
</section>
`;

export default boxing;
