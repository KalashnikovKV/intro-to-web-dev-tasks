const feature = () => `
<section class="Feature">
<div class="feature-describe-h3">
  <h3 class="feature-describe-h3-h3">Good headphones and loud music is all you need</h3>
  <div class="feature-describe">
    <div class="describe">
      <div class="describe-img">
        <div class="decribe-img-container">
          <img class="feature-img-icon" src="/packages/hometask-beats/assets/icons/battery.svg" alt="battery">
          <img class="feature-img-el27" src="/packages/hometask-beats/assets/icons/Ellipse27.svg" alt="Ellipse">
          <img class="feature-img-el26" src="/packages/hometask-beats/assets/icons/Ellipse26.svg" alt="Ellipse">
        </div>
      </div>
      <img class="feature-img-el25" src="/packages/hometask-beats/assets/icons/Ellipse25.svg" alt="Ellipse">
      <div class="describe-1-1">
        <h3>Battery</h3>
        <p>Battery 6.2V-AAC codec</p>
        <a href="#">Lern More</a>
      </div>
    </div>
    <div class="describe">
      <div class="describe-img">
        <div class="decribe-img-container">
          <img class="feature-img-icon" src="/packages/hometask-beats/assets/icons/bluetooth.svg" alt="bluetooth">
          <img class="feature-img-el27" src="/packages/hometask-beats/assets/icons/Ellipse27.svg" alt="Ellipse">
          <img class="feature-img-el26" src="/packages/hometask-beats/assets/icons/Ellipse26.svg" alt="Ellipse">
        </div>
      </div>
      <img class="feature-img-el25" src="/packages/hometask-beats/assets/icons/Ellipse25.svg" alt="Ellipse">
      <div class="describe-1-1">
        <h3>Bluetooth</h3>
        <p>Battery 6.2V-AAC codec</p>
        <a href="#">Lern More</a>
      </div>
    </div>
    <div class="describe">
      <div class="describe-img">
        <div class="decribe-img-container">
          <img class="feature-img-icon" src="../../../assets/icons/microphone.svg" alt="microphone">
          <img class="feature-img-el27" src="../../../assets/icons/Ellipse27.svg" alt="Ellipse">
          <img class="feature-img-el26" src="../../../assets/icons/Ellipse26.svg" alt="Ellipse">
        </div>
      </div>
      <img class="feature-img-el25" src="../../../assets/icons/Ellipse25.svg" alt="Ellipse">
      <div class="describe-1-1">
        <h3>Microphone</h3>
        <p>Battery 6.2V-AAC codec</p>
        <a href="#">Lern More</a>
      </div>
    </div>
  </div>
</div>
<div class="feature-desctribe-img">
  <div class="feature-img"><img src="../../../assets/images/feature.png" alt="feature"></div>
</div>
</section>
`;

export default feature;
