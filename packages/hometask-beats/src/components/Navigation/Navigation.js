const header = () => `
<header>
<div class="container">
  <a class="logo" href="#"><img src="../../../assets/icons/logo.svg" alt="logo"></a>
  <nav>
    <ul>
      <li class="li-header"><img src="../../../assets/icons/search.svg" alt="search.svg"></li>
      <li class="li-header"><img src="../../../assets/icons/box.svg" alt="box.svg"></li>
      <li class="li-header"><img src="../../../assets/icons/user.svg" alt="user.svg"></li>
    </ul>
  </nav>
  <button class="mobile-header-menu"><img src="../../../assets/icons/menu.svg" alt="menu"></button>
</div>
</header>
`;

export default header;
