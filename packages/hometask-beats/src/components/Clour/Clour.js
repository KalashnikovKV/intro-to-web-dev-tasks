const clour = () => `
<section class="Clour">
<h3>Our Latest colour collection 2021</h3>
<div class="clour-slider">
  <div class="left-str"><img src="../../../assets/icons/arrow-left.svg" alt="left-str"></div>
  
  <div class="center-sld"><img src="../../../assets/images/clour-01.png" alt="clour-01"></div>
  <div class="left-right-sld">
    <div class="left-sld"><img src="../../../assets/images/Rectangle74.png" alt="clour-02"></div>
    <div class="right-sld"><img src="../../../assets/images/clour-03.png" alt="clour-03"></div>
  </div>
  
  <div class="right-str"><img src="../../../assets/icons/arrow-right.svg" alt="right-str"></div>
</div>
</section>
`;

export default clour;
