const product = () => `
<section class="Product">
<h3>Our Latest Product</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis nunc ipsum aliquam, ante. </p>
<div class="product-desc-img">
  <div class="product-1">
    <div class="product-images">
      <div class="product-images-1">
        <div class="product-02"><img src="/packages/hometask-beats/assets/images/product-03.png" alt="product-01.png"></div>
        <div class="background-product-01"></div>
      </div>
      <div class="product-images-2">
        <div class="shopping-cart">
          <img src="/packages/hometask-beats/assets/icons/shopping-cart.svg" alt="shopping-cart">
        </div>
        <img src="/packages/hometask-beats/assets/icons/Ellipse28.svg" alt="Ellipse28">
      </div>
    </div>
    <div class="product-description">
      <div class="product-description-1">
        <div class="stars">
          <img class="" src="/packages/hometask-beats/assets/icons/Star1.svg" alt="Star1">
          <img class="" src="/packages/hometask-beats/assets/icons/Star1.svg" alt="Star1">
          <img class="" src="/packages/hometask-beats/assets/icons/Star1.svg" alt="Star1">
          <img class="" src="/packages/hometask-beats/assets/icons/Star1.svg" alt="Star1">
          <img class="" src="/packages/hometask-beats/assets/icons/Star1.svg" alt="Star1">
        </div>
        <span>4.50</span>
      </div>
      <div class="product-description-2">
        <p>Blue Headphone</p>
        <span>$ 235</span>
      </div>
    </div>
  </div>
  <div class="product-2">
    <div class="product-images">
      <div class="product-images-1">
        <div class="product-02"><img src="/packages/hometask-beats/assets/images/product-02.png" alt="product-02.png"></div>
        <div class="background-product-02"></div>
      </div>
      <div class="product-images-2">
        <div class="shopping-cart">
          <img src="/packages/hometask-beats/assets/icons/shopping-cart.svg" alt="shopping-cart">
        </div>
        <img src="/packages/hometask-beats/assets/icons/Ellipse28.svg" alt="Ellipse28">
      </div>
    </div>
    <div class="product-description">
      <div class="product-description-1">
        <div class="stars">
          <img class="" src="/packages/hometask-beats/assets/icons/Star1.svg" alt="Star1">
          <img class="" src="/packages/hometask-beats/assets/icons/Star1.svg" alt="Star1">
          <img class="" src="/packages/hometask-beats/assets/icons/Star1.svg" alt="Star1">
          <img class="" src="/packages/hometask-beats/assets/icons/Star1.svg" alt="Star1">
          <img class="" src="/packages/hometask-beats/assets/icons/Star1.svg" alt="Star1">
        </div>
        <span>4.50</span>
      </div>
      <div class="product-description-2">
        <p>Blue Headphone</p>
        <span>$ 235</span>
      </div>
    </div>
  </div>
  <div class="product-3">
    <div class="product-images">
      <div class="product-images-1">
        <div class="product-02"><img src="/packages/hometask-beats/assets/images/product-01.png" alt="product-03.png"></div>
        <div class="background-product-03"></div>
      </div>
      <div class="product-images-2">
        <div class="shopping-cart">
          <img src="/packages/hometask-beats/assets/icons/shopping-cart.svg" alt="shopping-cart">
        </div>
        <img src="/packages/hometask-beats/assets/icons/Ellipse28.svg" alt="Ellipse28">
      </div>
    </div>
    <div class="product-description">
      <div class="product-description-1">
        <div class="stars">
          <img class="" src="../../../assets/icons/Star1.svg" alt="Star1">
          <img class="" src="../../../assets/icons/Star1.svg" alt="Star1">
          <img class="" src="../../../assets/icons/Star1.svg" alt="Star1">
          <img class="" src="../../../assets/icons/Star1.svg" alt="Star1">
          <img class="" src="../../../assets/icons/Star1.svg" alt="Star1">
        </div>
        <span>4.50</span>
      </div>
      <div class="product-description-2">
        <p>Blue Headphone</p>
        <span>$ 235</span>
      </div>
    </div>
  </div>
</div>
</section>
`;

export default product;
