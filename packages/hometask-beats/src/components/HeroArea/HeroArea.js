const heroArea = () => `
<section class="HeroArea">
<div class="image-hero"><img src="../../../assets/images/hero.png" alt="image-hero"></div>
<div class="hero-content">
  <span>Hear it. Feel it</span>
  <h2 class="hero-content-h2">Move with the music</h2>
  <div class="group32">
    <h2>$ 435</h2>
    <div class="line"></div>
    <h3>$ 465</h3>
  </div>
  <button>BUY NOW</button>
</div>
</section>
`;

export default heroArea;
