const futear = () => `
<section class="Futear">
<div class="futear-logo"><img src="../../../assets/icons/logo.svg" alt="logo"></div>
<nav class="futear-nav">
  <ul>
    <li class="li-futear">Home</li>
    <li class="li-futear">About</li>
    <li class="li-futear">Product</li>
  </ul>
</nav>
<div class="futear-icons">
  <div class="futear-icons-1"><img src="../../../assets/icons/instagram.svg" alt="instagram.svg"></div>
  <div class="futear-icons-1"><img src="../../../assets/icons/twitter.svg" alt="twitter.svg"></div>
  <div class="futear-icons-1"><img src="../../../assets/icons/facebook.svg" alt="facebook.svg"></div>
</div>
</section>
`;

export default futear;
