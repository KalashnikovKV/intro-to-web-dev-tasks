import Boxing from "./components/Boxing/Boxing";
import Clour from "./components/Clour/Clour";
import Feature from "./components/Feature/Feature";
import Futear from "./components/Futear/Futear";
import HeroArea from "./components/HeroArea/HeroArea";
import MainCta from "./components/MainCta/MainCta";
import Navigation from "./components/Navigation/Navigation";
import Product from "./components/Product/Product";

const app = document.querySelector(".app");

const render = () => {
  app.innerHTML = `
  ${Navigation()}
  ${HeroArea()}
  ${Clour()}
  ${Feature()}
  ${Product()}
  ${Boxing()}
  ${MainCta()}
  ${Futear()}
  `;
};

export default render;
